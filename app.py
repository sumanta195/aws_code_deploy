from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
   return {"response":"Hello world"}


@app.route('/about')
def hello():
   return {"response":"About us"}

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=8080, debug=True)